##ТЕСТОВОЕ ЗАДАНИЕ

#код листенера находится в src/listener.py

telegram: dane4kq
Козлов Даниил Денисович


обязательно пропишите .env.docker файл

вот какой был у меня 
`
DB_HOST=db
DB_PORT=5432
POSTGRES_DB=postgres
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
JWT_SECRET=secret123
API_HOST=web
API_PORT=8000
KAFKA_HOST=broker
KAFKA_PORT=9092
`



Сначала поднимаем наши контенеры
`docker-compose up --build -d`

обязательно дожидемся пока кафка поднимится и создаем топик
`docker-compose exec broker kafka-topics --create --topic Ptopic --bootstrap-server broker:9092`

:8000/api/v1/message/ - создать сообщение
:8000/api/v1/confirm_message/ - создать сообщение