from django.db import models
from djchoices import DjangoChoices, ChoiceItem


class MessageStatus(DjangoChoices):
    review = ChoiceItem(value="review", label="на проверке")
    blocked = ChoiceItem(value="blocked", label="заблокировано")
    correct = ChoiceItem(value="correct", label="корректно")


class Message(models.Model):
    status = models.CharField(choices=MessageStatus, default=MessageStatus.review, max_length=50)
    message = models.CharField(max_length=255)
    user_id = models.IntegerField()
