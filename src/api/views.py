# from django.shortcuts import render
# from kafka import KafkaProducer
# import pickle

# Create your views here.
import pickle

import jwt
from jwt import DecodeError
from kafka3 import KafkaProducer
from rest_framework import viewsets, mixins
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from api.serializers import MessageSerializer, MessageConfirmSerializer
from django.conf import settings


class MessageModelCreateView(viewsets.GenericViewSet, mixins.CreateModelMixin):
    serializer_class = MessageSerializer

    def create(self, request, *args, **kwargs):
        resp = super().create(request, *args, **kwargs)

        producer = KafkaProducer(security_protocol="PLAINTEXT",
                                 bootstrap_servers=f'{settings.KAFKA_HOST}:{settings.KAFKA_PORT}')
        v = {
            'msg': {
                "message_id": resp.data['id'],
                "text": resp.data['message']
            }
        }
        serialized_data = pickle.dumps(v, pickle.HIGHEST_PROTOCOL)
        producer.send('Ptopic', serialized_data)
        return resp


class JWTPermission(BasePermission):
    def has_permission(self, request, view):
        token_ = request.headers['Authorization']
        if not token_:
            return False
        try:
            decoded_token = jwt.decode(token_, settings.JWT_SECRET, algorithms=["HS256"])
        except DecodeError:
            return False
        if decoded_token.get('role') == 'post_message_confirm':
            return True
        return False


class MessageConfirmView(viewsets.GenericViewSet, mixins.UpdateModelMixin):
    serializer_class = MessageConfirmSerializer
    permission_classes = [JWTPermission, ]

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.update(serializer.validated_data)

        return Response(status=200, data={"success": True})
