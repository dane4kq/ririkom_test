
from rest_framework import serializers

from api.models import Message


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'user_id', 'message')
        read_only_fields = ('id',)


class MessageConfirmSerializer(serializers.Serializer):
    message_id = serializers.IntegerField()
    success = serializers.BooleanField()

    def update(self, validated_data):
        if validated_data['success']:
            status = 'correct'
        else:
            status = 'blocked'
        print(status)
        return Message.objects.filter(id=validated_data['message_id']).update(status=status)
