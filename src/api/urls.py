from django.urls import path

from api.views import MessageModelCreateView, MessageConfirmView

urlpatterns = [
    path("message/",
         MessageModelCreateView.as_view(actions={"post": "create"})),
    path("message_confirmation/",
         MessageConfirmView.as_view(actions={"post": "update"})),
]
