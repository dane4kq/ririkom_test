import os
import pickle
import jwt
import requests
from dotenv import load_dotenv, find_dotenv
from kafka3 import KafkaConsumer

load_dotenv(find_dotenv())
JWT_SECRET = os.environ.get('JWT_SECRET', 'secret')
API_HOST = os.environ.get('API_HOST', 'localhost')
API_PORT = os.environ.get('API_PORT', '8000')
KAFKA_HOST = os.environ.get('KAFKA_HOST', 'localhost')
KAFKA_PORT = os.environ.get('KAFKA_PORT', '9092')

consumer = KafkaConsumer('Ptopic',
                         bootstrap_servers=[f'{KAFKA_HOST}:{KAFKA_PORT}'],
                         api_version=(0, 10)
                         )

payload = {"role": "post_message_confirm"}
headers = {
    'Authorization': jwt.encode(payload, JWT_SECRET, algorithm="HS256")
}

while True:
    for message in consumer:
        deserialized_data = pickle.loads(message.value)
        text = deserialized_data.get('msg').get('text')
        message_id = deserialized_data.get('msg').get('message_id')

        success = False if 'абракадабра' in text.lower() else True
        body = {
            "success": success,
            "message_id": message_id
        }
        requests.post(f'http://{API_HOST}:{API_PORT}/api/v1/message_confirmation/', data=body, headers=headers)
